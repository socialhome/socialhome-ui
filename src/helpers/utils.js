import { useFetch} from "@vueuse/core"

export function extractMentions(text) {
    return Array.from(text.matchAll(/@([\w\-.]+@[\w\-.]+\.[A-Za-z0-9]+)[\W\s]?/g), (m) => m[1].toLowerCase())
}

const serverErrors = {
    "204": "",
    "500": "Internal Server Error",
    "501": "Not Implemented",
    "502": "Bad Gateway",
    "503": "Service Unavailable",
    "504": "Gateway Timeout"
}

export async function apiFetch(apiEndpoint, fetchOptions, callback)  {
    const { data, error, response, statusCode } = await useFetch(apiEndpoint, fetchOptions)
    let errorMessage = error.value
    if (!errorMessage) {
        if (statusCode.value !== 204) callback(JSON.parse(data.value))
    }
    else if (response.value) {
        if (response.value?.headers.get('content-type') === 'application/json') {
            const detail =  await response.value.json()
            console.log('detail', detail)
            errorMessage = detail?.message ?? detail?.detail ?? detail?.recipients?.slice(0)[0] ?? detail[0] ?? "Unknown Error"
        }
        else errorMessage = serverErrors[response.value?.status.toString()] ?? "Unknown Error"
        console.log('apiFetch', response, errorMessage)
    } else errorMessage = serverErrors[statusCode.value.toString()] ?? "Unknown Error"
    console.log('apiFetch', response, statusCode.value, errorMessage)
    return { errorMessage, statusCode: statusCode.value }
}