import './index.css'
import 'primeicons/primeicons.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import PrimeVue from 'primevue/config'
import ToastService from 'primevue/toastservice'
import ConfirmationService from 'primevue/confirmationservice'
import { useAuthStore } from '@/stores/auth.js'
import App from './App.vue'
import router from './routes.js'
import MasonryWall from '@yeger/vue-masonry-wall'
import { OhVueIcon, addIcons } from "oh-vue-icons"
import {
    BiCaretDown,
    BiCaretUpFill,
    BiCheck2,
    BiChevronExpand,
    BiGlobe2,
    BiPerson,
    FaCamera,
    FaCode,
    FaLink,
    FaListOl,
    FaListUl,
    FaQuoteLeft,
    FaRegularImage,
    HiCheck,
    HiHome,
    HiLockClosed,
    HiUserGroup,
    HiSolidMinus,
    IoChatbubbles,
    IoPersonOutline,
    LaEllipsisVSolid } from "oh-vue-icons/icons"
   // import aura from "@/presets/aura"

addIcons(
    BiCaretDown,
    BiCaretUpFill,
    BiCheck2,
    BiChevronExpand,
    BiGlobe2,
    BiPerson,
    FaCamera,
    FaCode,
    FaLink,
    FaListOl,
    FaListUl,
    FaQuoteLeft,
    FaRegularImage,
    HiCheck,
    HiHome,
    HiLockClosed,
    HiUserGroup,
    HiSolidMinus,
    IoChatbubbles,
    IoPersonOutline,
    LaEllipsisVSolid)

const app = createApp(App)
const pinia = createPinia()
app.use(MasonryWall)
app.component("v-icon", OhVueIcon)
app.use(PrimeVue, {
    theme: 'none'
})
app.use(ToastService)
app.use(ConfirmationService)
app.use(pinia)
const authStore = useAuthStore()
authStore.getAuthenticatedUser().then(() => {
    app.use(router)
    router.isReady().then(() => {app.mount('#app')})
})
