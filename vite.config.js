import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  // eslint-disable-next-line no-undef
  const env = loadEnv(mode, process.cwd(), '')
  return {
    plugins: [
      vue(),
    ],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    server: {
      hmr: {
        path: '/hmr'
      },
      //proxy: {
      //  '/api': {
      //    target: env.VITE_API_URL,
      //    changeOrigin: true,
      //    toProxy: true
      //  },
      //  '/ch': {
      //    target: env.VITE_API_URL,
      //    changeOrigin: true,
      //    toProxy: true,
      //    ws: true,
      //  },
      //},
    }
  }
})
